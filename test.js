setTimeout(() => { console.log('timeout last') }, 2 )
setTimeout(() => { console.log('timeout1') } )
setTimeout(() => { console.log('timeout2') } )
                  
Promise.resolve()
  .then(() => { console.log('promise') })

setTimeout(() => { console.log('timeout3') } )
setTimeout(() => { console.log('timeout4') } )
setTimeout(() => { console.log('timeout5') } )

let i = 0

function testInterval() {
  let startDate = +new Date()
  let interval = setInterval(
    () => {
      const endDate = +new Date()
      if ((endDate - startDate) === 20) {
        console.log('first interval')
      }
      if ((endDate - startDate) === 1000) {
        clearInterval(interval);
        console.log(i)
        return
      }
      i++;
    }, 5
  )
}

testInterval()
testInterval()

