class Col {
    constructor(elementContainer, index) {
        this.elementContainer = elementContainer;
        this.index = index
        this.cards = [];
    }

    addCards(cards) {
        cards.forEach(card => {
            card.col = this;
            this.cards.push(card);
        })
    }

    delCard(len) {
        this.cards.splice(-len)
        if (this.cards.every(card => card.closed)) this.render()
    }

    getAllCardsAfterIndex(index) {
        return this.cards.slice(index, this.cards.length)
            .filter(card => !card.closed)
    }

    render(dontChangeClosing) {
        const { elementContainer, cards } = this;
        elementContainer.innerHTML = ''

        cards.forEach((card, index) => {
            if (!dontChangeClosing) card.setClosed(index < cards.length - 1)
            card.indexInCol = index
            card.isEndOfCol = index == this.cards.length < 1
            card.render()
        })

        if (cards.length == 0) {
            elementContainer.innerHTML = '<div class="card back-gray empty"></div>'
        }
    }
}


export default Col