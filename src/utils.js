function uuidv4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
}

function shuffle(array) {
    array.sort(() => Math.random() - 0.5);
}

function get(obj, key, defaultValue = null) {
    if (obj && obj[key]) return obj[key];
    return defaultValue
}

export {
    uuidv4,
    getRandomInt,
    shuffle,
    get
}
