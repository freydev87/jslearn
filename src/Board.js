import Card from './Card'
import Col from './Col'
import Deck from './Deck'

import { shuffle, get } from './utils'

class Board {
    constructor() {
        this.cards = this.generateCards();
        this.selectedCards = [];
        this.cols = [
            new Col(document.querySelector('#col1'), 1),
            new Col(document.querySelector('#col2'), 2),
            new Col(document.querySelector('#col3'), 3),
            new Col(document.querySelector('#col4'), 4),
            new Col(document.querySelector('#col5'), 5),
            new Col(document.querySelector('#col6'), 6),
            new Col(document.querySelector('#col7'), 7),
        ];

        this.deck = new Deck(
            document.querySelector('.deck'),
            document.querySelector('.top-card')
        )

        shuffle(this.cards)
        this.deck.addCards(this.cards)
        this.deck.render()

        this.dealCards()
        this.renderCols()

        this.aces = [
            [], // 0
            [], // 1
            [], // 2
            [], // 3
        ]

        document.addEventListener('click', event => {
            const targetCardElement = event.target
            const targetId = targetCardElement.getAttribute('id')
            const previousCard = this.selectedCards[0] || {}

            if (targetCardElement.className.indexOf('empty') > -1) {
                if (previousCard.value == 13) {
                    const id = targetCardElement.closest('.col').getAttribute('id').replace('col', '')
                    const col = this.cols[id - 1]

                    this.deck.deleteIfExists(previousCard)                    
                    if (previousCard.col) {
                        previousCard.col.delCard(this.selectedCards.length)
                        previousCard.col.render()
                    }
                    col.addCards(this.selectedCards)
                    col.render(true)
                    this.clearSelectedCards()
                }
            }

            if (targetCardElement.closest('.ace') && this.selectedCards.length === 1) {
                const aceBoxIndex = targetCardElement.closest('.ace').getAttribute('index')
                const aceBoxArray = this.aces[aceBoxIndex]
                const isEmpty = aceBoxArray.length === 0

                if (isEmpty && (get(previousCard, 'value') == 1)) { 
                    aceBoxArray.push(previousCard)
                    if (previousCard.col) {
                        previousCard.col.delCard(1)
                        previousCard.col.render(true)
                    }
                    this.deck.deleteIfExists(previousCard)
                    this.clearSelectedCards()
                    this.renderAces()
                }

                if (!isEmpty && this.selectedCards.length === 1 &&
                    previousCard.suit === aceBoxArray[aceBoxArray.length - 1].suit &&
                    previousCard.value == (+aceBoxArray[aceBoxArray.length - 1].value) + 1
                    ) {
                        aceBoxArray.push(previousCard)
                        if (previousCard.col){
                            previousCard.col.delCard(1)
                            previousCard.col.render(true)
                        }
                        this.clearSelectedCards()
                        this.deck.deleteIfExists(previousCard)
                        this.renderAces()
                    }
            }

            if (targetId) {
                const currentCard = this.getCardById(targetId)
                if (!currentCard) return

                if (targetId == previousCard.id) { 
                    this.clearSelectedCards()
                    return
                }

                if (!currentCard.closed && !(this.findCardInAceBox(currentCard.id) > -1 && currentCard.value == 1)) {
                    if (previousCard.id) {
                        if (
                            currentCard.indexInCol !== null &&
                            get(previousCard.col, 'index') != get(currentCard.col, 'index') &&
                            currentCard.color != previousCard.color &&
                            (parseInt(previousCard.value) + 1) == currentCard.value
                        ) {  
                            if (previousCard.col) {

                                previousCard.col.delCard(this.selectedCards.length)
                                previousCard.col.render(true)
                            }

                            this.deleteIfExists(previousCard.id)
                            currentCard.col.addCards(this.selectedCards)
                            currentCard.col.render(true)
                            this.clearSelectedCards()
                            this.deck.deleteIfExists(previousCard)
                        } else {
                            this.setselectedCards(
                                currentCard.col ? currentCard.col.getAllCardsAfterIndex(currentCard.indexInCol) : [currentCard]
                            )
                        }
                    } else {
                        this.setselectedCards(
                            currentCard.col ? currentCard.col.getAllCardsAfterIndex(currentCard.indexInCol) : [currentCard]
                        )
                    }
                }
            }
        })

    }

    getCardById(id) {
        return this.cards.find(card => card.id === id)
    }

    findCardInAceBox(id) {
        let resultIndex;

        this.aces.forEach((aceBox, index) => {
            const found = aceBox.find(card => card.id == id)
            if (found) resultIndex = index
        })

        return resultIndex
    }

    deleteIfExists(id) {        
        const index = this.findCardInAceBox(id)
        if (index > -1) {
            this.aces[index].splice(-1);
            this.renderAces()
        }
    }

    renderAces() {
        const aceElements = document.querySelectorAll('.ace')
        aceElements.forEach((elem, index) => {
            elem.innerHTML = ''
            if (this.aces[index].length > 0) elem.classList.remove('opacity-5')
            this.aces[index].forEach(card => {
                card.col = null
                card.render(elem)
            })
        })
    }

    dealCards() {
        let currentCol = this.cols[6]
        this.deck.leftCards.forEach((card, index) => {
            if (index < 28) {
                if (index > 6) currentCol = this.cols[5]
                if (index > 12) currentCol = this.cols[4]
                if (index > 17) currentCol = this.cols[3]
                if (index > 21) currentCol = this.cols[2]
                if (index > 24) currentCol = this.cols[1]
                if (index > 26) currentCol = this.cols[0]

                currentCol.addCards([card])
                delete this.deck.leftCards[index]
            }
        })

        this.deck.leftCards = this.deck.leftCards.filter(card => card)
    }

    setselectedCards(cards) {
        this.cards.forEach(card => card.setActive(false))
        this.selectedCards = cards
        cards.forEach(card =>  card.setActive(true))
    }

    clearSelectedCards() {
        this.selectedCards = []
        this.cards.forEach(card => card.setActive(false))
    }

    generateCards() {
        const suits = ['club', 'spade', 'diamond', 'heart']
        const values = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13']

        const cards =
            [].concat.apply([],
                suits.map(suit => values.map(value => new Card(suit, value, this))))

        return cards
    }

    renderCols() {
        this.cols.forEach(col => col.render())
    }
}

export default Board