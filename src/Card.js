import { uuidv4, get } from './utils'

const SUIT_COLORS = {
    'heart': 'red',
    'diamond': 'red',
    'spade': 'black',
    'club': 'black'
}          

class Card {
    constructor(suit, value, board) {
        this.suit = suit;
        this.color = SUIT_COLORS[suit];
        this.value = value;
        this.closed = false;

        this.col = null;
        this.indexInCol = null;
        this.isEndOfCol = null;

        this.id = uuidv4()
        this.board = board
    }

    setClosed(closed) {
        this.closed = closed
    }

    get cardName() {
        const { value, suit } = this;

        return `${suit}-${value}`
    }
    
    setActive(active) {
        const found = document.querySelector(`[id="${this.id}"]`)
        if (found) found.classList[active ? 'add' : 'remove']('active')
    }

    render(cardContainer) {
        (cardContainer || this.col.elementContainer).innerHTML += `
            <div id="${this.id}" class="card ${this.closed ? 'back-blue' : this.cardName}"></div>
        `
    }
}

export default Card 