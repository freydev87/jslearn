class Deck {
    constructor(elementContainer, rightCardContainer) {
        this.elementContainer = elementContainer
        this.leftCards = []
        this.rightCards = []
        this.rightCardContainer = rightCardContainer

        elementContainer.addEventListener('click', this.handleClick.bind(this))
    }

    handleClick(event) {
        if (this.leftCards.length === 0) {
            this.leftCards = this.rightCards.slice(0).reverse()
            this.rightCards.length = 0
        }

        const currentCard = this.leftCards.pop()
        if (currentCard) this.rightCards.push(currentCard)

        this.render()
    }

    deleteIfExists(card) {
        const foundIndex = this.rightCards.findIndex(c =>
            c.suit === card.suit &&
            c.value === card.value
        )

        if (foundIndex > -1) {
            this.rightCards.splice(-1)
            this.render()
        }
    }

    addCards(card) {
        this.leftCards = this.leftCards.concat(card)
    }

    render() {
        const { rightCardContainer, rightCards, elementContainer, leftCards } = this
        elementContainer.innerHTML = `` +
            (leftCards.length >= 1 ? `<div class="card back-blue"></div>` : '') +
            (leftCards.length >= 8 ? `<div class="card back-blue"></div>` : '') +
            (leftCards.length >= 16 ? `<div class="card back-blue"></div>` : '') +
            (leftCards.length === 0 ? `<div class="card back-gray"></div>` : '')
            
        rightCardContainer.innerHTML = ''
        rightCards.forEach(card =>
            card.render(rightCardContainer)
        )
    }

    pop() {
        return this.leftCards.pop()
    }
}

export default Deck
